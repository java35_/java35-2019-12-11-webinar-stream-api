import java.util.Arrays;

public class Example1Appl {

	public static void main(String[] args) {
		int[] array = {120, 410, 85, 32, 314, 12};
		
		imperativeStyle0(array);
		System.out.println("******************");
		imperativeStyle1(array);
		System.out.println("******************");
		imperativeStyle2(array);
		System.out.println("******************");
		functionalStyle(array);
	}

	private static void functionalStyle(int[] array) {
		Arrays.stream(array)
				.filter(x -> x < 300)
				.map(x -> x + 11)
				.limit(3)
				.forEach(x -> System.out.println(x));
	}

	private static void imperativeStyle0(int[] array) {
		int[] resArray = new int[3];
		int count = 0;
		
		for (int i = 0; i < array.length && count < 3; i++) {
			if (array[i] < 300)
				resArray[count++] = array[i] + 11;
		}
		
		for (int value : resArray) {
			System.out.println(value);
		}
	}
	
	private static void imperativeStyle1(int[] array) {
		int count = 0;
		for (int i = 0; i < array.length && count < 3; i++) {
			if (array[i] < 300) {
				System.out.println(array[i] + 11);
				count++;
			}
		}
	}
	
	private static void imperativeStyle2(int[] array) {
		int count = 0;
		for (int value : array) {
			if (value >= 300)
				continue;
			value += 11;
			count++;
			if (count > 3)
				break;
			System.out.println(value);
		}
	}
}
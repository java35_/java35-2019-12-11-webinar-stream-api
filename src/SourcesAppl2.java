import java.util.Arrays;
import java.util.stream.Stream;

public class SourcesAppl2 {

	public static void main(String[] args) {
		Stream.of(1, 2, 3).forEach(x -> System.out.println(x));
		System.out.println("************");
		
		int[] array = {1, 2, 3};
//		Stream.of(array).forEach(x -> System.out.println(x));
		
		Arrays.asList(1, 2, 3).stream().forEach(x -> System.out.println(x));
		Arrays.stream(array).forEach(x -> System.out.println(x));
		
		System.out.println("**************");
		Stream.ofNullable(null).forEach(x -> System.out.println(x));
		
		Stream.builder()
				.add(1)
				.add(2)
				.add(3)
				.build()
				.forEach(x -> System.out.println(x));
	}

}

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TerminalOperators {
	public static void main(String[] args) {
		IntSummaryStatistics statistic = 
					IntStream.of(1, 2, 3)
								.summaryStatistics();
		System.out.println(statistic.getAverage());
		System.out.println(statistic.getCount());
		System.out.println(statistic.getMax());
		System.out.println(statistic.getMin());
		
		System.out.println("******************");
		System.out.println(IntStream.of(1, 2, 3).count());
		
		System.out.println("******************");
		int[] array = IntStream.of(1, 2, 3).toArray();
		
		for (int value : array) {
			System.out.println(value);
		}
		
		System.out.println("***********************");
		List<String> list = new ArrayList<>();
		list.add("hi");
		list.add("hello");
		
		Set<String> set = list.stream()
						.collect(Collectors.toSet());
		set.forEach(x -> System.out.println(x));
	}
}
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class SourcesAppl {
	public static void main(String[] args) {
		Set<Integer> mySet = new HashSet<>();
		
		mySet.add(3);
		mySet.add(4);
		mySet.add(5);
		mySet.add(6);
		mySet.add(7);
		
		mySet.stream()
				.filter(x -> x < 7)
				.map(x -> x + 11)
				.limit(3)
				.forEach(x -> System.out.println(x));
		
		List<Integer> myList = new ArrayList<>();
		
		myList.add(3);
		myList.add(4);
		myList.add(5);
		myList.add(6);
		myList.add(7);
		
		myList.stream()
				.filter(x -> x < 7)
				.map(x -> x + 11)
				.limit(3)
				.forEach(x -> System.out.println(x));
		
		
		System.out.println("**************************");
		Map<Integer, String> map = new HashMap<>();
		
		map.put(1, "1");
		map.put(2, "2");
		map.put(3, "3");
		map.put(4, "4");
		map.put(5, "5");
		map.put(6, "6");
		
		map.entrySet().stream()
						.filter(x -> x.getKey() < 4)
//						.map(x -> {
//								x.setValue(x.getValue() + "-" + x.getKey());
//								return x;
//							})
						.map(x -> x.getValue())
						.limit(3)
						.forEach(x -> System.out.println(x));
//						.forEach(x -> System.out.println(x.getKey() + " : " + x.getValue()));
		System.out.println("******************");
		map.forEach((x, y) -> System.out.println(x + " : " + y));
		
		
		map.values().stream();
		map.keySet().stream();
	}
}

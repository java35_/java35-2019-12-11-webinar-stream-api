import java.util.stream.IntStream;
import java.util.stream.Stream;

public class OPerators {
	public static void main(String[] args) {
//		Stream.of(1, 2, 3)
		IntStream.of(1, 2, 3, 3)
				.skip(1)
				.peek(x -> System.out.println(x))
				.mapToLong(x -> x)
				.peek(x -> System.out.println(x))
				.distinct()
				.forEach(x -> System.out.println(x));
	}
}

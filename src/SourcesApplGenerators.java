import java.util.Random;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class SourcesApplGenerators {

	public static void main(String[] args) {
		// Random
		new Random().ints()
					.limit(5)
					.forEach(x -> System.out.println(x));
		System.out.println("******************");
		Stream.empty()
				.forEach(x -> System.out.println(x));
		
		Stream.generate(() -> 6)
				.limit(3)
				.forEach(x -> System.out.println(x));
		System.out.println("**************");
		IntStream.range(5, 10)
					.forEach(x -> System.out.println(x));
//		DoubleStream
//		LongStream
		System.out.println("**************");
		IntStream.rangeClosed(5, 10)
					.forEach(x -> System.out.println(x));
	}
}